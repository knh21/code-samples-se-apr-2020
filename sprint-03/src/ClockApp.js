import React from "react";
import "./App.css";
import Clock from "./Clock";

function ClockApp() {
  return (
    <div className="App">
      <Clock />
    </div>
  );
}

export default ClockApp;
