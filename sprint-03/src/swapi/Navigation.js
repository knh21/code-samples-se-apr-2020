import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { SwapiContext } from "../hooks/useSwapi";
import Loader from "./Loader";

/* Nico starts here */
const Navigation = () => {
  const [state, actions] = useContext(SwapiContext);

  useEffect(() => {
    // fetch root resources on mount =]
    actions.fetchRoot();
    // eslint-disable-next-line
  }, []);

  if (state.loaders.root) {
    return <Loader />;
  }

  console.log("NavigationComponent", state.root);

  return (
    <>
      {state.root.map((resource) => (
        <Link key={resource} to={`/${resource}`}>
          {resource}
        </Link>
      ))}
    </>
  );
};

export default Navigation;
