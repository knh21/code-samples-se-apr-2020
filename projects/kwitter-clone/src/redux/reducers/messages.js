import produce from "immer";
import {
  LOAD_MESSAGES,
  LOAD_MESSAGES_SUCCESS,
  LOAD_MESSAGES_FAILURE,
  CREATE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_SUCCESS,
  TOGGLE_MESSAGE_LIKE_SUCCESS,
  CREATE_MESSAGE_FAILURE,
  DELETE_MESSAGE_FAILURE,
  TOGGLE_MESSAGE_LIKE_FAILURE,
} from "../actions/messages";

// INITIAL STATE
const INITIAL_STATE = {
  data: {},
  loading: false,
  error: "",
};

/*

{
  "id": 0,
  "text": "string",
  "username": "string",
  "createdAt": "2020-09-20T18:57:37.031Z",
  "isLiked": false,
  "likes": [
    {
      "id": 0,
      "username": "string",
      "messageId": 0,
      "createdAt": "2020-09-20T18:57:37.031Z"
    }
  ]
}

*/

/*
  immer best practices for mutating data
  https://github.com/immerjs/immer/issues/115#issue-301648058
*/
export const messagesReducer = (state = { ...INITIAL_STATE }, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case LOAD_MESSAGES: {
        draft.loading = true;
        draft.error = "";
        break;
      }
      case LOAD_MESSAGES_SUCCESS: {
        action.payload.forEach((message) => {
          draft.data[message.id] = message;
        });
        draft.error = "";
        break;
      }
      case LOAD_MESSAGES_FAILURE: {
        draft.error = action.payload;
        draft.loading = false;
        break;
      }
      case CREATE_MESSAGE_SUCCESS: {
        draft.data[action.payload.id] = action.payload;
        draft.data[action.payload.id].isLiked = false;
        draft.error = "";
        break;
      }
      case CREATE_MESSAGE_FAILURE: {
        draft.error = "Unable to create message";
        break;
      }
      case DELETE_MESSAGE_SUCCESS: {
        delete draft.data[action.payload];
        draft.error = "";
        break;
      }
      case DELETE_MESSAGE_FAILURE: {
        draft.error = "Unable to delete message";
        break;
      }
      case TOGGLE_MESSAGE_LIKE_SUCCESS: {
        const nextMessage = action.payload.freshMessage;
        nextMessage.isLiked = !state.data[action.payload.messageId].isLiked;
        draft.data[action.payload.messageId] = nextMessage;
        draft.error = "";
        break;
      }
      case TOGGLE_MESSAGE_LIKE_FAILURE: {
        draft.error = "Unable to like message";
        break;
      }
      default:
        return draft;
    }
  });
