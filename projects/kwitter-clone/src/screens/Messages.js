import React from "react";
import { MessageListContainer, MenuContainer } from "../components";

export const MessagesScreen = () => (
  <>
    <MenuContainer />
    <MessageListContainer />
  </>
);
