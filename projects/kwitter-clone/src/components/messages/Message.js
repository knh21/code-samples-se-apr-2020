import React from "react";
import { Col, Card, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { actions as messageActions } from "../../redux/actions/messages";

export const Message = (props) => {
  const message = useSelector((state) => state.message.data[props.id]);
  const dispatch = useDispatch();

  const buttonInfo = React.useMemo(() => {
    const variant = message.isLiked ? "primary" : "secondary";
    const text = message.isLiked ? "Unlike" : "Like";
    return {
      variant,
      text,
    };
  }, [message]);

  const onLikeUnlikeButtonPress = () => {
    dispatch(messageActions.toggleMessageLike(props.id));
  };

  return (
    <Col md={4}>
      <Card style={{ width: "18rem" }}>
        <Card.Img variant="top" src="holder.js/100px180" />
        <Card.Body>
          <Card.Text>{message.text}</Card.Text>
          <Button
            variant={buttonInfo.variant}
            onClick={onLikeUnlikeButtonPress}
          >
            {buttonInfo.text}
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
};
