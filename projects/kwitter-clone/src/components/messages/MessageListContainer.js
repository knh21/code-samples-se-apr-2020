import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Row } from "react-bootstrap";
import { actions as messageActions } from "../../redux/actions/messages";
import { Message } from "./Message";

export const MessageListContainer = () => {
  const messages = useSelector((state) => Object.keys(state.message.data));
  const messageError = useSelector((state) => state.message.error);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(messageActions.loadMessages());
  }, [dispatch]);

  return (
    <Container>
      {messageError ? <h4>{messageError}</h4> : null}
      <Row>
        {messages.map((messageId) => (
          <Message key={`message-component-${messageId}`} id={messageId} />
        ))}
      </Row>
    </Container>
  );
};
