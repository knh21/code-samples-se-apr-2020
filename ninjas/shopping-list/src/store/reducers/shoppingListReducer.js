import shoppingListItemsBetter from "../../data/shopping-list-better.json";
import {
  SHOPPING_LIST_ADD,
  SHOPPING_LIST_REMOVE,
  SHOPPING_LIST_CHANGE_FILTER,
  SHOPPING_LIST_LOADING,
} from "../actions/shoppingList";

const initialState = {
  items: shoppingListItemsBetter,
  currentFilter: "",
  loading: false,
};

// add item to items key
// remove item from items key
// change the filter

const shoppingListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SHOPPING_LIST_REMOVE:
      const nextItems = {};
      // payload here is the string of the id of the element we want omitted
      for (const [key, value] of Object.entries(state.items)) {
        if (key === payload) {
          // skip the item that we want to remove
          return;
        }
        nextItems[key] = value;
      }
      return {
        ...state,
        items: nextItems,
      };
    case SHOPPING_LIST_ADD:
      return {
        ...state,
        items: {
          ...state.items,
          [payload.id]: payload,
        },
      };
    case SHOPPING_LIST_CHANGE_FILTER:
      return {
        ...state,
        currentFilter: payload,
      };

    case SHOPPING_LIST_LOADING:
      return {
        ...state,
        loading: payload,
      };
    default:
      return state;
  }
};

export default shoppingListReducer;
