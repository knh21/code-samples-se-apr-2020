import { sleep } from "../../utils";
export const SHOPPING_LIST_ADD = "SHOPPING_LIST_ADD";
export const SHOPPING_LIST_REMOVE = "SHOPPING_LIST_REMOVE";
export const SHOPPING_LIST_CHANGE_FILTER = "SHOPPING_LIST_CHANGE_FILTER";
export const SHOPPING_LIST_LOADING = "SHOPPING_LIST_LOADING";

// payload is an object which is the item we want to add
const addItem = (payload) => {
  return {
    type: SHOPPING_LIST_ADD,
    payload,
  };
};

// payload is the string of the id we want removed
const removeItem = (payload) => {
  return {
    type: SHOPPING_LIST_REMOVE,
    payload,
  };
};

// payload is a string of the new filter
const changeFilter = (payload) => {
  return {
    type: SHOPPING_LIST_CHANGE_FILTER,
    payload,
  };
};

// payload is the value of if we are loading or not (boolean)
const setLoading = (payload) => {
  return {
    type: SHOPPING_LIST_LOADING,
    payload,
  };
};

const addItemToCart = (payload) => {
  // this returned function is our thunk
  return async (dispatch) => {
    // tell our reducer we are loading
    dispatch(setLoading(true));
    // trick students into thinking this is actually taking time to do its thing
    await sleep(3);
    // add the item to the reducer
    dispatch(addItem(payload));
    // stop loading
    dispatch(setLoading(false));
  };
};

const removeItemFromCart = (payload) => {
  // this returned function is our thunk
  return async (dispatch) => {
    // tell our reducer we are loading
    dispatch(setLoading(true));
    // trick students into thinking this is actually taking time to do its thing
    await sleep(3);
    // add the item to the reducer
    dispatch(removeItem(payload));
    // stop loading
    dispatch(setLoading(false));
  };
};

export const actions = {
  changeFilter,
  addItemToCart,
  removeItemFromCart,
};
