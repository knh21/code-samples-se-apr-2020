import { combineReducers } from "redux";
import counterReducer from "./counter";

/*

{
  counter: 0,
  auth: {
    user: null,
    isLoggedIn: false
  },
  github: {
    repos: [],
  }
}

*/

const reducer = combineReducers({
  counter: counterReducer,
  // ....
});

export default reducer;
