import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/analytics";

const app = firebase.initializeApp({
  apiKey: "AIzaSyDz19ECnCdbD3b-c3PguT_TcjYPCUMW0AI",
  authDomain: "fir-auth-demo-d11c1.firebaseapp.com",
  databaseURL: "https://fir-auth-demo-d11c1.firebaseio.com",
  projectId: "fir-auth-demo-d11c1",
  storageBucket: "fir-auth-demo-d11c1.appspot.com",
  messagingSenderId: "77470951897",
  appId: "1:77470951897:web:f5bad80e3566eeffa491c8",
  measurementId: "G-36GPRB429N",
});

// Initialize Firebase
firebase.analytics();
export default app;
