require("dotenv").config();
import dotenv from "dotenv";
import { MongoClient } from "mongodb";
import util from "util";

// Connection URL
const url = process.env.MONGO_URL || "mongodb://localhost:27017";

// Database Name
const dbName = "students";

const connectToMongo = util.promisify(MongoClient.connect);

async function start() {
  try {
    const client = await connectToMongo(url);
    // the client should be connected here

    const db = client.db(dbName);
    console.log("It works still!");
  } catch (err) {
    console.error("Unable to connect to MongoDB", err);
  }
}

start();

// // Use connect method to connect to the server
// MongoClient.connect(url, function (err, client) {
//   if (err) {
//     console.error("FML", err);
//     process.exit(1);
//   }
//   console.log("Connected successfully to server");

//   const db = client.db(dbName);

//   client.close();
// });
