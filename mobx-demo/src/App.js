import React from "react";
import { observer } from "mobx-react";
import store from "./store";

const ACounter = observer(() => {
  return (
    <>
      <p>Hello World</p>
      <p>Clicked {store.counter} times</p>
      <button onClick={() => store.increment()}>Inc</button>
      <button onClick={store.decrement}>Dec</button>
      <h3>ALL TODOS</h3>
      <ul>
        {store.todos.map((todo) => (
          <li key={todo.name} onClick={() => store.toggleTodo(todo.id)}>
            {todo.name}
          </li>
        ))}
      </ul>

      <h3>Completed TODOS</h3>
      <ul>
        {store.completedTodos.map((todo) => (
          <li key={todo.name}>{todo.name}</li>
        ))}
      </ul>
    </>
  );
});

export default observer(() => (
  <>
    <ACounter />
  </>
));
